#ifndef _TEXTURE_H
#define _TEXTURE_H

#include "CView2d.h"

class GUI_API CTexture : public CView2d
{
public:
	CTexture(int x, int y, int width, int height, SQChar* file);

	Sqrat::Table getPosition();
	void setPosition(int x, int y);

	Sqrat::Table getPositionPx();
	void setPositionPx(int x, int y);

	Sqrat::Table getSize();
	void setSize(int width, int height);

	Sqrat::Table getSizePx();
	void setSizePx(int width, int height);

	Sqrat::Table getRect();
	void setRect(int x, int y, int width, int height);

	Sqrat::Table getRectPx();
	void setRectPx(int x, int y, int width, int height);

	Sqrat::Table getColor();
	void setColor(unsigned char r, unsigned char g, unsigned char b);

	unsigned char getAlpha();
	void setAlpha(unsigned char alpha);

	SQChar* getFile();
	void setFile(SQChar* file);

	// Inherited (Required because Sqrat throws assertion if bound member method is inherited and not implemented)

	bool getVisible() { return CView::getVisible(); }
	void setVisible(bool visible) { CView::setVisible(visible); }

	void top() { CView::Top(); }

private:
	zVEC2 _uvPos;
	zVEC2 _uvSize;

	virtual void Blit() override;
};

#endif