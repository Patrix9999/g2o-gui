#ifndef _VIEW2D_H
#define _VIEW2D_H

#include "CView.h"

class GUI_API CView2d : public CView
{
public:
	CView2d(int x, int y);

	int getVirtualX();
	void setVirtualX(int x);

	int getVirtualY();
	void setVirtualY(int y);

	int getPixelX();
	void setPixelX(int x);

	int getPixelY();
	void setPixelY(int y);

	int getVirtualWidth();
	void setVirtualWidth(int width);

	int getVirtualHeight();
	void setVirtualHeight(int height);

	int getPixelWidth();
	void setPixelWidth(int width);

	int getPixelHeight();
	void setPixelHeight(int height);

protected:
	zVEC2 _virtualPosition;
	zVEC2 _pixelPosition;
	zVEC2 _virtualSize;
	zVEC2 _pixelSize;
};

#endif
