#ifndef _DRAW_3D_H
#define _DRAW_3D_H

#include "CView2d.h"

class GUI_API CDraw3d : private CView2d
{
public:
	CDraw3d(float x, float y, float z);

	Sqrat::Table getWorldPosition();
	void setWorldPosition(float x, float y, float z);

	Sqrat::Table getColor();
	void setColor(unsigned char r, unsigned char g, unsigned char b);

	unsigned char getAlpha();
	void setAlpha(unsigned char alpha);

	void insertText(SQChar* text);
	void removeText(SQInteger position);
	void updateText(SQInteger position, SQChar* text);
	Sqrat::Array getText();

	SQChar* getFont();
	void setFont(SQChar* fontName);

	SQInteger getDistance();
	void setDistance(SQInteger distance);

	// Inherited (Required because Sqrat throws assertion if bound member method is inherited and not implemented)

	bool getVisible() { return CView::getVisible(); }
	void setVisible(bool visible) { CView::setVisible(visible); }

	void top() { CView::Top(); }

private:
	zVEC3 _position;
	int _distance;
	std::vector<std::string> _text;

	void updateSize();
	virtual void Blit() override;
};

#endif