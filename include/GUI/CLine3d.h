#ifndef _LINE_3D_H
#define _LINE_3D_H

#include "CView.h"

class GUI_API CLine3d : private CView
{
public:
	CLine3d(int beginX, int beginY, int beginZ, int endX, int endY, int endZ);

	Sqrat::Table getBegin();
	void setBegin(SQFloat x, SQFloat y, SQFloat z);

	Sqrat::Table getEnd();
	void setEnd(SQFloat x, SQFloat y, SQFloat z);

	Sqrat::Table getColor();
	void setColor(unsigned char r, unsigned char g, unsigned char b);

	// Inherited (Required because Sqrat throws assertion if bound member method is inherited and not implemented)

	bool getVisible() { return CView::getVisible(); }
	void setVisible(bool visible) { CView::setVisible(visible); }

	void top() { CView::Top(); }

private:
	zVEC3 _begin;
	zVEC3 _end;

	virtual void Blit() override;
};

#endif