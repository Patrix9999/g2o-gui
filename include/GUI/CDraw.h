#ifndef _DRAW_H
#define _DRAW_H

#include "CView2d.h"

class GUI_API CDraw : private CView2d
{
public:
	CDraw(int x, int y, SQChar* text);

	Sqrat::Table getPosition();
	void setPosition(int x, int y);

	Sqrat::Table getPositionPx();
	void setPositionPx(int x, int y);

	int getWidth();
	int getWidthPx();

	int getHeight();
	int getHeightPx();

	Sqrat::Table getColor();
	void setColor(unsigned char r, unsigned char g, unsigned char b);

	unsigned char getAlpha();
	void setAlpha(unsigned char alpha);

	SQChar* getText();
	void setText(SQChar* text);

	SQChar* getFont();
	void setFont(SQChar* fontName);

	// Inherited (Required because Sqrat throws assertion if bound member method is inherited and not implemented)

	bool getVisible() { return CView::getVisible(); }
	void setVisible(bool visible) { CView::setVisible(visible); }

	void top() { CView::Top(); }

private:
	zSTRING _text;
	
	void updateSize();
	virtual void Blit() override;
};

#endif