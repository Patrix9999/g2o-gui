#include "CItemRender.h"

using namespace sqModule;

zCWorld* CItemRender::_renderWorld = nullptr;
zCCamera* CItemRender::_renderCamera = nullptr;

CItemRender::CItemRender(int x, int y, int width, int height, SQChar* instance) : CView2d(x, y),
	_lightRange(5000),
	_lightingSwell(false),
	_instance(instance)
{
	_cameraVob = new zCVob();
	_item = new oCItem(zSTRING(instance), 1);

	// Lazy initialization of static render world
	if (!_renderWorld)
	{
		_renderWorld = zfactory->CreateWorld();

		_renderWorld->bspTree.bspTreeMode = zBSP_MODE_INDOOR;
		_renderWorld->activeSkyControler->fillBackground = FALSE;
	}

	// Lazy initialization of static render camera
	if (!_renderCamera)
		_renderCamera = new zCCamera();

	setSize(width, height);
}

CItemRender::~CItemRender()
{
	zRELEASE(_cameraVob);
	zRELEASE(_item);
}

Sqrat::Table CItemRender::getPosition()
{
	return Sqrat::Table(vm)
		.SetValue("x", getVirtualX())
		.SetValue("y", getVirtualY());
}

void CItemRender::setPosition(int x, int y)
{
	setVirtualX(x);
	setVirtualY(y);
}

Sqrat::Table CItemRender::getPositionPx()
{
	return Sqrat::Table(vm)
		.SetValue("x", getPixelX())
		.SetValue("y", getPixelY());
}

void CItemRender::setPositionPx(int x, int y)
{
	setPosition(screen->anx(x), screen->any(y));
}

Sqrat::Table CItemRender::getSize()
{
	return Sqrat::Table(vm)
		.SetValue("width", getVirtualWidth())
		.SetValue("height", getVirtualHeight());
}

void CItemRender::setSize(int width, int height)
{
	setVirtualWidth(width);
	setVirtualHeight(height);
}

Sqrat::Table CItemRender::getSizePx()
{
	return Sqrat::Table(vm)
		.SetValue("width", getPixelWidth())
		.SetValue("height", getPixelHeight());
}

void CItemRender::setSizePx(int width, int height)
{
	setSize(screen->anx(width), screen->any(height));
}

SQChar* CItemRender::getInstance()
{
	return _instance;
}

void CItemRender::setInstance(SQChar* instance)
{
	_instance = instance;

	_item->InitByScript(parser->GetIndex(_instance), 1);
	_item->SetVisual(NULL);
}

SQInteger CItemRender::getRotX()
{
	return _item->inv_rotx;
}

void CItemRender::setRotX(int rotX)
{
	_item->inv_rotx = rotX;
}

SQInteger CItemRender::getRotY()
{
	return _item->inv_roty;
}

void CItemRender::setRotY(int rotY)
{
	_item->inv_roty = rotY;
}

SQInteger CItemRender::getRotZ()
{
	return _item->inv_rotz;
}

void CItemRender::setRotZ(int rotZ)
{
	_item->inv_rotz = rotZ;
}

SQInteger CItemRender::getZBias()
{
	return _item->inv_zbias;
}

void CItemRender::setZBias(int zBias)
{
	_item->inv_zbias = zBias;
}

SQInteger CItemRender::getLightRange()
{
	return _lightRange;
}

void CItemRender::setLightRange(int lightRange)
{
	_lightRange = lightRange;
}

bool CItemRender::getLightingSwell()
{
	return _lightingSwell;
}

void CItemRender::setLightingSwell(bool lightingSwell)
{
	_lightingSwell = lightingSwell;
}

SQChar* CItemRender::getVisual()
{
	return _item->file.ToChar();
}

void CItemRender::setVisual(SQChar* visual)
{
	_item->file = visual;
	_item->SetVisual(NULL);
}

void CItemRender::Blit()
{
	if (!_visible)
		return;

	// setting up pixel values (required)
	pposx = getPixelX();
	pposy = getPixelY();
	psizex = getPixelWidth();
	psizey = getPixelHeight();

	zCCamera* oldCamera = zCCamera::activeCam;

	zTRnd_AlphaBlendFunc oldBlendFunc = zrenderer->GetAlphaBlendFunc();
	zrenderer->SetAlphaBlendFunc(zRND_ALPHA_FUNC_NONE);

	int oldLightRange = playerLightInt;
	playerLightInt = _lightRange;

	int oldLightingSwell = oCItem::GetLightingSwell();
	oCItem::SetLightingSwell(!_lightingSwell);

	//// Add item to world
	_item->SetPositionWorld({ 0, 0, 0 });
	_item->RotateForInventory(1);
	_renderWorld->AddVob(_item);

	//// Add camera to world
	_renderCamera->connectedVob = _cameraVob;
	_renderWorld->AddVob(_cameraVob);

	//// Render init
	_item->RenderItemPlaceCamera(_renderCamera, TRUE);
	_renderCamera->SetRenderTarget(this);

	//// Render
	zCEventManager::disableEventManagers = TRUE;
	_renderWorld->Render(*_renderCamera);
	zCEventManager::disableEventManagers = FALSE;

	// Render cleanup
	_renderWorld->RemoveVob(_cameraVob);
	_renderWorld->RemoveVobSubtree(_item);

	// Restoring old values
	zCCamera::activeCam = oldCamera;
	zrenderer->SetAlphaBlendFunc(oldBlendFunc);
	playerLightInt = oldLightRange;
	oCItem::SetLightingSwell(oldLightingSwell);
}
