#include "CView2d.h"

CView2d::CView2d(int x, int y) :
	_virtualSize(0, 0),
	_pixelSize(0, 0)
{
	setVirtualX(x);
	setVirtualY(y);
}

int CView2d::getVirtualX()
{
	return _virtualPosition[VX];
}

void CView2d::setVirtualX(int x)
{
	_virtualPosition[VX] = x;
	_pixelPosition[VX] = screen->nax(x);
}

int CView2d::getVirtualY()
{
	return _virtualPosition[VY];
}

void CView2d::setVirtualY(int y)
{
	_virtualPosition[VY] = y;
	_pixelPosition[VY] = screen->nay(y);
}

int CView2d::getPixelX()
{
	return _pixelPosition[VX];
}

void CView2d::setPixelX(int x)
{
	_pixelPosition[VX] = x;
	_virtualPosition[VX] = screen->anx(x);
}

int CView2d::getPixelY()
{
	return _pixelPosition[VY];
}

void CView2d::setPixelY(int y)
{
	_pixelPosition[VY] = y;
	_virtualPosition[VY] = screen->any(y);
}

int CView2d::getVirtualWidth()
{
	return _virtualSize[VX];
}

void CView2d::setVirtualWidth(int width)
{
	_virtualSize[VX] = width;
	_pixelSize[VX] = screen->nax(width);
}

int CView2d::getVirtualHeight()
{
	return _virtualSize[VY];
}

void CView2d::setVirtualHeight(int height)
{
	_virtualSize[VY] = height;
	_pixelSize[VY] = screen->nay(height);
}

int CView2d::getPixelWidth()
{
	return _pixelSize[VX];
}

void CView2d::setPixelWidth(int width)
{
	_pixelSize[VX] = width;
	_virtualSize[VX] = screen->anx(width);
}

int CView2d::getPixelHeight()
{
	return _pixelSize[VY];
}

void CView2d::setPixelHeight(int height)
{
	_pixelSize[VY] = height;
	_virtualSize[VY] = screen->any(height);
}