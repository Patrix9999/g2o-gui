#include "CTexture.h"

using namespace sqModule;

CTexture::CTexture(int x, int y, int width, int height, SQChar* file) : CView2d(x, y),
	_uvPos(0.0f, 0.0f),
	_uvSize(1.0f, 1.0f)
{
	setSize(width, height);
	setFile(file);
}

Sqrat::Table CTexture::getPosition()
{
	return Sqrat::Table(vm)
		.SetValue("x", getVirtualX())
		.SetValue("y", getVirtualY());
}

void CTexture::setPosition(int x, int y)
{
	setVirtualX(x);
	setVirtualY(y);
}

Sqrat::Table CTexture::getPositionPx()
{
	return Sqrat::Table(vm)
		.SetValue("x", getPixelX())
		.SetValue("y", getPixelY());
}

void CTexture::setPositionPx(int x, int y)
{
	setPixelX(x);
	setPixelY(y);
}

Sqrat::Table CTexture::getSize()
{
	return Sqrat::Table(vm)
		.SetValue("width", getVirtualWidth())
		.SetValue("height", getVirtualHeight());
}

void CTexture::setSize(int width, int height)
{
	setVirtualWidth(width);
	setVirtualHeight(height);
}

Sqrat::Table CTexture::getSizePx()
{
	return Sqrat::Table(vm)
		.SetValue("width", getPixelWidth())
		.SetValue("height", getPixelHeight());
}

void CTexture::setSizePx(int width, int height)
{
	setPixelWidth(width);
	setPixelHeight(height);
}

Sqrat::Table CTexture::getRect()
{
	return Sqrat::Table(vm)
		.SetValue("width", _uvSize[VX] * getVirtualWidth())
		.SetValue("height", _uvSize[VY] * getVirtualHeight());
}

void CTexture::setRect(int x, int y, int width, int height)
{
	int virtualWidth = getVirtualWidth();
	int virtualHeight = getVirtualHeight();

	_uvPos[VX] = static_cast<float>(x) / virtualWidth;
	_uvPos[VY] = static_cast<float>(y) / virtualHeight;
	_uvSize[VX] = _uvPos[VX] + static_cast<float>(width) / virtualWidth;
	_uvSize[VY] = _uvPos[VY] + static_cast<float>(height) / virtualHeight;
}

Sqrat::Table CTexture::getRectPx()
{
	return Sqrat::Table(vm)
		.SetValue("width", _uvSize[VX] * getPixelWidth())
		.SetValue("height", _uvSize[VY] * getPixelHeight());
}

void CTexture::setRectPx(int x, int y, int width, int height)
{
	setRect(screen->anx(x), screen->any(y), screen->anx(width), screen->any(height));
}

Sqrat::Table CTexture::getColor()
{
	return Sqrat::Table(vm)
		.SetValue("r", color.r)
		.SetValue("g", color.g)
		.SetValue("b", color.b);
}

void CTexture::setColor(unsigned char r, unsigned char g, unsigned char b)
{
	color.SetRGB(r, g, b);
}

unsigned char CTexture::getAlpha()
{
	return color.a;
}

void CTexture::setAlpha(unsigned char alpha)
{
	color.a = alpha;
}

SQChar* CTexture::getFile()
{
	if (!backTex)
		return "";

	return backTex->GetObjectName().ToChar();
}

void CTexture::setFile(SQChar* file)
{
	InsertBack(file);
}

void CTexture::Blit()
{
	if (!backTex)
		return;

	if (!_visible)
		return;

	// Get positions of upper left (posMin) and lower right (posMax) vertices
	zVEC2 posMin = zVEC2(getPixelX(), getPixelY());
	zVEC2 posMax = zVEC2(posMin[VX] + getPixelWidth(), posMin[VY] + getPixelHeight());

	// if element is outside the screen -> break
	if (posMin[VX] > zrenderer->vid_xdim - 1 || posMin[VY] > zrenderer->vid_ydim - 1)
		return;

	if (posMax[VX] < 0 || posMax[VY] < 0)
		return;

	zREAL onScreenPosMinX = max(posMin[VX], 0);
	zREAL onScreenPosMinY = max(posMin[VY], 0);
	zREAL onScreenPosMaxX = min(posMax[VX], zrenderer->vid_xdim - 1);
	zREAL onScreenPosMaxY = min(posMax[VY], zrenderer->vid_ydim - 1);

	// calculating element size on screen
	zREAL onScreenSizeWidth = onScreenPosMaxX - onScreenPosMinX;
	zREAL onScreenSizeHeight = onScreenPosMaxY - onScreenPosMinY;

	// if element is invisible (upperLeft == lowerRight) -> break
	if (onScreenSizeWidth <= 0 || onScreenSizeHeight <= 0)
		return;

	zrenderer->SetViewport(onScreenPosMinX, onScreenPosMinY, onScreenSizeWidth, onScreenSizeHeight);

	zBOOL oldzWrite = zrenderer->GetZBufferWriteEnabled();
	zrenderer->SetZBufferWriteEnabled(m_bFillZ);

	zTRnd_ZBufferCmp oldCmp = zrenderer->GetZBufferCompare();
	zrenderer->SetZBufferCompare(zRND_ZBUFFER_CMP_ALWAYS);

	zTRnd_AlphaBlendFunc oldBlendFunc = zrenderer->GetAlphaBlendFunc();
	zrenderer->SetAlphaBlendFunc(alphafunc);

	zBOOL oldBilerpFilter = zrenderer->GetBilerpFilterEnabled();
	zrenderer->SetBilerpFilterEnabled(oldBilerpFilter);

	zREAL farZ;
	if (m_bFillZ)
		farZ = (zCCamera::activeCam) ? zCCamera::activeCam->farClipZ - 1 : 65534;
	else
		farZ = (zCCamera::activeCam) ? zCCamera::activeCam->nearClipZ + 1 : 1;

	zrenderer->DrawTile(backTex, posMin, posMax, farZ, _uvPos, _uvSize, color);

	// Restoring old values (defaults in zrenderer)
	zrenderer->SetBilerpFilterEnabled(oldBilerpFilter);
	zrenderer->SetAlphaBlendFunc(oldBlendFunc);
	zrenderer->SetZBufferWriteEnabled(oldzWrite);
	zrenderer->SetZBufferCompare(oldCmp);
}
